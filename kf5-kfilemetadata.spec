%undefine __cmake_in_source_build
%global framework kfilemetadata

%global         ffmpeg 1
%global         catdoc 1
%global         ebook 1
%global         poppler 1
%global         taglib 1

Name:           kf5-%{framework}
Summary:        A Tier 2 KDE Framework for extracting file metadata
Version:        5.116.0
Release:        1

License:        BSD-3-Clause AND CC0-1.0 AND LGPL-2.1-only AND LGPL-2.1-or-later AND LGPL-3.0-only AND (LGPL-2.1-only OR LGPL-3.0-only)
URL:            https://cgit.kde.org/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

# filter plugin provides
%global __provides_exclude_from ^(%{_kf5_plugindir}/.*\\.so)$

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-karchive-devel >= %{majmin}
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-ki18n-devel >= %{majmin}
BuildRequires:  kf5-rpm-macros
# optional
BuildRequires:  kf5-kconfig-devel >= %{majmin}

BuildRequires:  qt5-qtbase-devel

BuildRequires:  libattr-devel
BuildRequires:  pkgconfig(exiv2) >= 0.20

## optional deps
%if 0%{?catdoc}
BuildRequires:  catdoc
Recommends:     catdoc
%endif
%if 0%{?ebook}
BuildRequires:  ebook-tools-devel
%endif
%if 0%{?ffmpeg}
BuildRequires:  ffmpeg-devel
%endif
%if 0%{?poppler}
BuildRequires:  pkgconfig(poppler-qt5)
%endif
%if 0%{?taglib}
BuildRequires:  pkgconfig(taglib) >= 1.9
%endif

%description
%{summary}.

%package devel
Summary:        Developer files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       qt5-qtbase-devel
%description devel
%{summary}.


%prep
%autosetup -n %{framework}-%{version} -p1


%build
%{cmake_kf5}
%{cmake_build}


%install
%{cmake_install}

%find_lang %{name} --all-name

mkdir -p %{buildroot}%{_kf5_plugindir}/kfilemetadata/writers/


%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}*
%{_kf5_libdir}/libKF5FileMetaData.so.*

%dir %{_kf5_plugindir}/kfilemetadata/
%{_kf5_plugindir}/kfilemetadata/kfilemetadata_*.so
%dir %{_kf5_plugindir}/kfilemetadata/writers/
%if 0%{?taglib}
%{_kf5_plugindir}/kfilemetadata/writers/kfilemetadata_taglibwriter.so
%endif

%files devel
%{_kf5_libdir}/libKF5FileMetaData.so
%{_kf5_libdir}/cmake/KF5FileMetaData
%{_kf5_includedir}/KFileMetaData/
%{_kf5_archdatadir}/mkspecs/modules/qt_KFileMetaData.pri


%changelog
* Fri Feb 14 2025 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- update verison to 5.116.0

* Tue Jan 14 2025 misaka00251 <liuxin@iscas.ac.cn> - 5.115.0-2
- Adapt the new cmake macros

* Mon Mar 04 2024 houhongxun <houhongxun@kylinos.cn> - 5.115.0-1
- update version to 5.115.0

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 5.113.0-1
- update verison to 5.113.0

* Thu Aug 03 2023 wangqia <wangqia@uniontech.com> - 5.108.0-1
- Update to upstream version 5.108.0

* Fri Feb 17 2023 peijiankang <peijiankang@kylinos.cn> - 5.100.0-1
- update verison to 5.100.0

* Wed Jul 13 2022 misaka00251 <misaka00251@misakanet.cn> - 5.97.0-1
- Init package
